﻿using System;
using System.Web;
using System.Web.Optimization;

namespace barbar
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725

       
        public static void RegisterBundles(BundleCollection bundles)
        {
           // BundleTable.EnableOptimizations = true; 
           

            bundles.Add(new StyleBundle("~/Content/css/MainStyle").Include(
                        "~/Content/css/jquery.ui.core.css",
                        "~/Content/css/jquery.ui.resizable.css",
                        "~/Content/css/jquery.ui.selectable.css",
                        "~/Content/css/jquery.ui.accordion.css",
                        "~/Content/css/jquery.ui.autocomplete.css",
                        "~/Content/css/jquery.ui.button.css",
                        "~/Content/css/jquery.ui.dialog.css",
                        "~/Content/css/jquery.ui.slider.css",
                        "~/Content/css/jquery.ui.tabs.css",
                        "~/Content/css/jquery.ui.datepicker.css",
                        "~/Content/css/jquery.ui.progressbar.css",
                        "~/Content/css/jquery.ui.theme.css",
                        "~/Content/css/site.css",
                        "~/Content/css/bootstrap.css",
                        "~/Content/css/pixel-admin.css",
                        "~/Content/css/rtl.css",
                        "~/Content/css/themes.css",
                        "~/Content/css/widgets.css",
                        "~/Content/css/pages.css"
                        
                        ));

            //bundles.Add(new StyleBundle("~/Content/style/css").Include(
            //           "~/Content/style/bootstrap.min.css",
            //           "~/Content/style/pages.css",
            //           "~/Content/style/pixel-admin.css",
            //           "~/Content/style/rtl.min.css",
            //           "~/Content/style/themes.css",
            //           "~/Content/style/widgets.css"));

            bundles.Add(new ScriptBundle("~/Scripts/top-js").Include(
               "~/Scripts/demo.js"
            
                ));
            bundles.Add(new ScriptBundle("~/Scripts/bottom-js").Include(
              "~/Scripts/jquery-2.0.3.js",
              "~/Scripts/bootstrap.js",
              "~/Scripts/pixel-admin.js"

               ));
        }

        
    }
}
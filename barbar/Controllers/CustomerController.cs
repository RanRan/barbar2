﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace barbar.Controllers
{
    public class CustomerController : Controller
    {
        //
        // GET: /Customer/
       
        public ActionResult Index()
        {
            return View();
        }
       // [Authorize(Roles = "Customer")]
        public ActionResult CustomerHome()
        {
            Public.LoginController m = new Public.LoginController();
            if (m.whoIsIt() != "customer") return Redirect("~/Public/Error4O4");
            return View();
        }

    }
}

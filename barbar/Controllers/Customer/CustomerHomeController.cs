﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace barbar.Controllers.Customer
{
    public class CustomerHomeController : Controller
    {
        public JsonResult customerhomedetail(){
            string username;
            try
            {
                username = HttpContext.Session["LoggedIn"].ToString();
            }
            catch (Exception e) { throw new Exception("لطفا وارد سایت شوید"); }

          try{

            Models.userContext porterage = new Models.userContext();
            Models.CustomerRequestContext request = new Models.CustomerRequestContext();
            Models.vehicleContext vehicle = new Models.vehicleContext();
            Models.packTypeContext pack = new Models.packTypeContext();
            Models.PricesContext prices = new Models.PricesContext();
            Models.DestiContext desti = new Models.DestiContext();
            Models.PorterageContext por = new Models.PorterageContext();
           
          //  int ID = (from d in porterage.barbar where d.UserName == username select d.).First();
            List<int> requestids=(from d in request.barbar where d.CustomerID==username select d.Id).ToList();
            string source = (from d in request.barbar where d.CustomerID == username select d.SourceAddress).First();
            IList<Dictionary<string , IList>> answer = new List<Dictionary<string , IList>>();
            foreach (var dd in requestids)
            {
                Dictionary<string, IList> ha = new Dictionary<string, IList>();
                IList idd = new List<int>();
                idd.Add(dd);
                    
                int porId = (from i in request.barbar where i.CustomerID == username select i.PorterageID).First();
                IList porname = (from i in por.barbar where i.Id == porId select i.Name).ToList();
                IList sourcedesti = (from d in request.barbar where d.Id == dd select new { d.SourceAddress, d.destiAddress }).ToList();
                IList status = (from d in request.barbar where d.Id == dd select d.Status).ToList();
                int vehicleid = (from d in request.barbar where d.Id == dd select d.VehicleId).First();
                IList vehicleName = (from d in vehicle.barbar where d.ID == vehicleid select new { d.Vehicle_Value, d.Vehicle_Weight }).ToList();
                int packtypeid = (from d in request.barbar where d.Id == dd select d.PackTypeId).First();
                IList packtypename = (from d in pack.barbar where d.Id == packtypeid select d.TypeName).ToList();
                IList destiId = (from d in request.barbar where d.Id == dd select d.price).ToList();
             //   IList price = (from d in prices.barbar where d.source == source && d.destination == destiId && d.vehicleID == vehicleid select d.price).ToList();
                ha.Add("SourceDesti", sourcedesti);
                ha.Add("VehicleName", vehicleName);
                ha.Add("PackType", packtypename);
                ha.Add("Price", destiId);
                ha.Add("porterageName", porname);
                ha.Add("requestid", idd);
                ha.Add("status", status);
                answer.Add(ha);
            }
                  return Json(answer, JsonRequestBehavior.AllowGet);
            }
                catch(Exception e){
                
                throw new Exception("درخواستی موجود نیست");
                }
                
            
           
    
          
        
        }




        public JsonResult customerInfo() {

            string username = HttpContext.Session["LoggedIn"].ToString();
            Models.userContext user = new Models.userContext();

            try
            {
                IList info = (from i in user.barbar where i.UserName == username select new { i.FirstName, i.LastName, i.UserName, i.PhoneNumber, i.Email, i.Address }).ToList();
                return Json(info, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e) {
                throw e;
            }
        
        
        
        }


        //
        // GET: /CustomerHome/
        [Authorize(Roles="Customer")]
        public ActionResult Index()
        {
            return View();
        }

    }
}

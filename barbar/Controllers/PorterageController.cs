﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace barbar.Controllers
{
    public class PorterageController : Controller
    {
        //
        // GET: /Porterage/ 

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PorterageHome()
        {
            ViewBag.Message = "home";
            if (Roles.IsUserInRole("Customer")) return Redirect("~/Public/Error4O4");

            return View();
        }
       
    }
}

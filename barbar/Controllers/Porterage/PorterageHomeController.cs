﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace barbar.Controllers
{
    public class PorterageHomeController : Controller
    {

        public JsonResult porteragehomeDetail() {

            string username;
            try
            {
                username = HttpContext.Session["LoggedIn"].ToString();
            }
            catch (Exception e) { return Json("E" , JsonRequestBehavior.AllowGet); }
            try{

            Models.userContext u = new Models.userContext();
            Models.PorterageContext porterage = new Models.PorterageContext();
            Models.CustomerRequestContext request = new Models.CustomerRequestContext();
            Models.vehicleContext vehicle = new Models.vehicleContext();
            Models.packTypeContext pack = new Models.packTypeContext();
            Models.PricesContext prices = new Models.PricesContext();
            Models.DestiContext desti = new Models.DestiContext();
           // string username = HttpContext.Session["LoggedIn"].ToString();
            int ID = (from d in porterage.barbar where d.UserName == username select d.Id).First();
            List<int> requestids=(from d in request.barbar where d.PorterageID==ID select d.Id).ToList();
            string source = (from d in porterage.barbar where d.UserName == username select d.Source).First();
            IList<Dictionary<string, IList>> answer = new List<Dictionary<string, IList>>();
            foreach (var dd in requestids)
            {
                Dictionary<string, IList> ha = new Dictionary<string, IList>();

                IList idd = new List<int>();
                idd.Add(dd);

               // int porId = (from i in request.barbar where i.CustomerID == username select i.PorterageID).First();
               int porid = (from i in porterage.barbar where i.UserName == username select i.Id).First();
               string porname1 = (from i in request.barbar where i.PorterageID == porid select i.CustomerID).First();
               IList porname = (from i in u.barbar where i.UserName == porname1 select new {i.FirstName , i.LastName }).ToList();
                IList status = (from d in request.barbar where d.Id == dd select d.Status).ToList();
                IList sourcedesti = (from d in request.barbar where d.Id == dd select new { d.SourceAddress, d.destiAddress }).ToList();
                int vehicleid = (from d in request.barbar where d.Id == dd select d.VehicleId).First();
                IList vehicleName = (from d in vehicle.barbar where d.ID == vehicleid select d.Vehicle_name).ToList();
                int packtypeid = (from d in request.barbar where d.Id == dd select d.PackTypeId).First();
                IList packtypename = (from d in pack.barbar where d.Id == packtypeid select d.TypeName).ToList();
                int destiId = (from d in request.barbar where d.Id == dd select d.DestinationId).First();
                IList price = (from d in prices.barbar where d.source == source && d.destination == destiId && d.vehicleID == vehicleid select d.price).ToList();
                ha.Add("SourceDesti", sourcedesti);
                ha.Add("VehicleName", vehicleName);
                ha.Add("PackType", packtypename);
                ha.Add("Price", price);
                ha.Add("porterageName", porname);
                ha.Add("requestid", idd);
                ha.Add("status", status);
                answer.Add(ha);
            }
                  return Json(answer, JsonRequestBehavior.AllowGet);
            }
                catch(Exception e){
                
                throw e;
                }
                
            
           
    
          
        
        }

        public JsonResult update(int id) {
            try
            {
            var username = HttpContext.Session["LoggedIn"];
            Models.userContext u = new Models.userContext();
            Models.CustomerRequestContext por = new Models.CustomerRequestContext();
            Models.PorterageContext p = new Models.PorterageContext();
            var name = (from i in p.barbar where i.UserName == username select i.Name).First();
            var request = (from i in por.barbar where i.Id == id select i.CustomerID).First();
            var email = (from i in u.barbar where i.UserName == request select i.Email).First();

            
            var it = por.barbar.Find(id);
            it.Status = 1;
            por.Entry(it).State = EntityState.Modified;
            por.SaveChanges();

           
                MailMessage mail = new MailMessage();


                mail.From = new MailAddress("talayeh.riahi@gmail.com");
                mail.To.Add(new MailAddress(email));
                mail.Subject = ("تایید باربری");
                mail.Body = " درخواست شما را تایید کرد بنابراین جهت عقد قرارداد به سایت مراجعه کنید "+ name +"باربری ";
                mail.IsBodyHtml = true;
                mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential("talayeh.riahi@gmail.com", "tala1374");// Enter seders User name and password
                smtp.EnableSsl = true;
                /*ServicePointManager.ServerCertificateValidationCallback =
                                        delegate(object s, X509Certificate certificate,
                                                 X509Chain chain, SslPolicyErrors sslPolicyErrors)
                                        { return true; };*/


                smtp.Send(mail);

                return null;

            }
            catch (Exception ee)
            {
                throw new Exception("مشکل در فرستادن ایمیل");
            }
            
        
        
        }
        public JsonResult getuser()
        {

            try
            {
                Models.PorterageContext p = new Models.PorterageContext();
                string username = HttpContext.Session["LoggedIn"].ToString();
                string name = (from i in p.barbar where i.UserName == username select i.Name).First();

                return Json(name, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e) { throw e; }

        }
        //
        // GET: /PorterageHome/

        public ActionResult PorterageHome()
        {
            return View();
        }

        //
        // GET: /PorterageHome/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /PorterageHome/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /PorterageHome/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /PorterageHome/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /PorterageHome/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /PorterageHome/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /PorterageHome/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

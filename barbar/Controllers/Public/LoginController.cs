﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;

namespace barbar.Controllers.Public
{
   

    public class LoginController : Controller
    {

        public String SignIn(string username, string password)
        {

            try
            {
                Models.userContext a = new Models.userContext();
                Models.PorterageContext b = new Models.PorterageContext();

           /*     var md = new HMACMD5();
                var salt = System.Text.Encoding.UTF8.GetBytes(password);
                var Pass1 = md.ComputeHash(salt);*/
             
                
                //  var u = a.barbar.Where<Models.User>(bb => bb.UserName == username && bb.Password == password);
                int u = (from aa in a.barbar where aa.UserName == username && aa.Password == password select aa.UserName).Count();
                var p = (from aa in b.barbar where aa.UserName == username && password == aa.Password select aa.Id).Count();
                if (u > 0)
                {
                    Session["loggedIn"] = username;
                    return "../Customer/CustomerHome";
                    //RedirectToAction("CustomerHome", CustomerController);//controler eshtebah
                }
                else if (p > 0)
                {
                    Session["loggedIn"] = username;
                    return "../Porterage/PorterageHome";
                }
                else
                    throw new Exception("Your username or Password is Invalid");

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        public string whoIsIt() {


            
         try
            {
             string username = HttpContext.Session["LoggedIn"].ToString();
                Models.userContext a = new Models.userContext();
                Models.PorterageContext b = new Models.PorterageContext();

                int u = (from aa in a.barbar where aa.UserName == username select aa.UserName).Count();
                var p = (from aa in b.barbar where aa.UserName == username  select aa.Id).Count();
                if (u > 0)
                {
                 
                    return "customer";
                    //RedirectToAction("CustomerHome", CustomerController);//controler eshtebah
                }
                else if (p > 0)
                {
                    
                    return "porterage";
                }
                else
                    throw new Exception("شما هنور وارد سایت نشده اید");

            }
         catch (Exception ex)
         {

             return "null";
         }
        
        
        
        }

        //
        // GET: /Login/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Login/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Login/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Login/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Login/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Login/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Login/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Login/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace barbar.Controllers.Public
{
    public class IndexController : Controller
    {
        //The parameters dictionary contains a null entry for parameter 'porId' of non-nullable type 'System.Int32' for method 'System.Web.Mvc.JsonResult request(Int32, Int32, Int32, Int32, System.String, System.String)' in 'barbar.Controllers.Public.PorterageInfoController'. An optional parameter must be a reference type, a nullable type, or be declared as an optional parameter.<br>Parameter name: parameters

        // GET: /Index/

        public ActionResult Index()
        {
           var a= HttpContext.Session["loggedIn"];
            return View();
           
        }
     
        public JsonResult Search(int weight,int value,int kind,int desti,string source)
        {
           // IList answer=null;
            Dictionary<string, IList> ans = new Dictionary<string, IList>();
              try
            {
                Models.PricesContext pc = new Models.PricesContext();
                IList price = (from i in pc.barbar where i.destination == desti && i.vehicleID == weight && i.source == source select i.price).ToList();
                //Roles.CreateRole("Customer");
                  
                //Roles.CreateRole("Porterage");
                //Roles.AddUserToRole(model.UserName, "haha");
                //Roles.IsUserInRole("haha");
                //Boolean temp = Roles.IsUserInRole("haha");
                //String[] s = Roles.GetUsersInRole("haha");
                // if(Roles.IsUserInRole("Customer")) return redirect 
                //  [Authorize(Roles="Customer")]

                Models.vehicleContext vehicle = new Models.vehicleContext();
                Models.PorterageContext porterage = new Models.PorterageContext();
                Models.packTypeContext packtype = new Models.packTypeContext();
                Models.PorterageVehicleContext porterageVehicle = new Models.PorterageVehicleContext();
                Models.PackTypePorterageContext packtypePorterage = new Models.PackTypePorterageContext();
                Models.DestiPorterageContext destiPorterage = new Models.DestiPorterageContext();
                Models.DestiContext destination = new Models.DestiContext();
                  
              //  var Kind_ID = (from i in packtype.barbar where i.Id == kind select i.Id).First();

                List<int> des = (from d in destiPorterage.barbar where d.Desti_ID == desti select d.Porterage_ID).ToList();
                List<int> veh = (from v in porterageVehicle.barbar where v.Vehicle_ID == weight && v.Vehicle_ID == value select v.Porterage_ID).ToList();
                List<int> Kind = (from k in packtypePorterage.barbar where k.IdPacktype == kind select k.IdPorterage).ToList();
                List<int> por = (from p in porterage.barbar where p.Source == source select p.Id).ToList();
               /* List<int> despor = new List<int>();
                  foreach(var d in des)
                     despor = (from dp in destiPorterage.barbar where  dp.Desti_ID == d select dp.Porterage_ID).ToList();

                  List<int> vehpor = new List<int>();
                  foreach (var d in veh)
                      vehpor = (from dp in porterageVehicle.barbar where dp.Vehicle_ID == d select dp.Porterage_ID).ToList();

                  List<int> kindpor = new List<int>();
                  foreach (var d in Kind)
                      kindpor = (from dp in packtypePorterage.barbar where dp.IdPacktype == d select dp.IdPorterage).ToList();*/
                  IList answer = des.Intersect(veh).Intersect(Kind).Intersect(por).ToList();
                  IList answeranswer = new List<IList>();
                  List<Models.Porterage> n = new List<Models.Porterage>();
                  foreach (int d in answer)
                  {
                      answeranswer.Add((from dp in porterage.barbar where dp.Id == d select new { dp.PhoneNumber, dp.Name, dp.Address, dp.Id }).ToList());
                      /*string phone = (from dp in porterage.barbar where dp.Id == d select dp.PhoneNumber).First();
                      string name = (from dp in porterage.barbar where dp.Id == d select dp.Name).First();
                      string add = (from dp in porterage.barbar where dp.Id == d select dp.Address).First();
                      int id=(from dp in porterage.barbar where dp.Id == d select dp.Id).First();
                      Models.Porterage k = new Models.Porterage(id,"0","",0,name,add,phone,"");
                     // Models.Porterage k =(Models.Porterage) (from dp in porterage.barbar where dp.Id == d select new { dp.PhoneNumber, dp.Name, dp.Address, dp.Id });
                      n.Add(k);*/

                  }

                  ans.Add("price", price);
                  ans.Add("porterages", answeranswer);
                  // IList sd = (from por in porterage.barbar join pp in packtypePorterage.barbar on por.Id equals pp.IdPorterage join pa in packtype.barbar on pp.IdPacktype equals pa.Id join vepo in porterageVehicle.barbar on por.Id equals vepo.Porterage_ID join ve in vehicle.barbar on vepo.Vehicle_ID equals ve.ID join depo in destiPorterage.barbar on por.Id equals depo.Porterage_ID join de in destination.barbar on depo.Desti_ID equals de.ID where ve.Vehicle_Weight == weight && ve.Vehicle_Value == value && de.Desti == desti && pa.Id == Kind_ID && por.Source == source select por.Name).ToList();
             //   IList sd = (from i in a.barbar join e in d.barbar on i.ID equals e.Vehicle_ID join s in b.barbar on e.Porterage_ID equals s.Id where i.Desti == desti && i.Source==source && i.Vehicle_Weight == weight && i.Vehicle_Value == value && i.Pack_Type_ID == Kind_ID select s.Name).ToList();
              
                /*foreach(var p in sd)
                {
                    answer = (from i in b.barbar where i.Id ==Convert.ToInt32( p) select i.Name).First();
                    //answer = b.barbar.SqlQuery("select Name from Porterage WHERE ID=p").ToList();
                }*/
                  return Json(ans, JsonRequestBehavior.AllowGet);
              
            }
              catch (Exception ex)
              {
                  throw ex;
              }
            
            
        }

        private void Intersect()
        {
            throw new NotImplementedException();
        }

        //
        // GET: /Index/Details/5

        public JsonResult Details()
        {

            //****************************************************************

            //Models.PricesContext p = new Models.PricesContext();
          

            //    Models.Prices pp = new Models.Prices(1 ,"اراک" , 192000 , 1);

            //    Models.Prices pp1 = new Models.Prices(1, "اراک", 2200000, 3);
            //    Models.Prices pp2 = new Models.Prices(1, "اراک", 270000, 5);
            //    Models.Prices pp3 = new Models.Prices(1, "اراک", 350000, 4);
            //    Models.Prices pp4 = new Models.Prices(1, "اراک", 490000, 6);
            //    Models.Prices pp5 = new Models.Prices(7, "اراک", 2100000, 1);
            //    Models.Prices pp6 = new Models.Prices(7, "اراک", 2700000, 3);
            //    Models.Prices pp7 = new Models.Prices(7, "اراک", 3400000, 5);
            //    Models.Prices pp8 = new Models.Prices(7, "اراک", 4400000, 4);
            //    Models.Prices pp9 = new Models.Prices(7, "اراک", 6400000, 6);
            //    Models.Prices pp10 = new Models.Prices(6, "اراک", 3500000, 1);
            //    Models.Prices pp11 = new Models.Prices(6, "اراک", 4000000, 3);
            //    Models.Prices pp12 = new Models.Prices(6, "اراک", 5300000, 5);
            //    Models.Prices pp13 = new Models.Prices(6, "اراک", 6300000, 4);
            //    Models.Prices pp14 = new Models.Prices(6, "اراک", 8700000, 6);
            //    p.barbar.Add(pp);
            //    p.barbar.Add(pp1);
            //    p.barbar.Add(pp2);
            //    p.barbar.Add(pp3);
            //    p.barbar.Add(pp4);
            //    p.barbar.Add(pp5);
            //    p.barbar.Add(pp6);
            //    p.barbar.Add(pp7);
            //    p.barbar.Add(pp8);
            //    p.barbar.Add(pp9);
            //    p.barbar.Add(pp10);
            //    p.barbar.Add(pp11);
            //    p.barbar.Add(pp12);
            //    p.barbar.Add(pp13);
            //    p.barbar.Add(pp14);
            //    p.SaveChanges();
           



            //****************************************************************

           // Models.PricesContext price = new Models.PricesContext();
            Models.packTypeContext pack = new Models.packTypeContext();
            Models.vehicleContext vehicle = new Models.vehicleContext();
            Models.DestiContext destii = new Models.DestiContext();
            Models.PorterageContext porterage = new Models.PorterageContext();

            try
            {

                IList desti = (from i in destii.barbar
                               select new
                               {
                                   i.ID,
                                   i.Desti,
                               }).ToList(); ;
                IList source = (from i in destii.barbar
                                select new
                                {
                                    i.ID,
                                    i.Desti,
                                }).ToList(); ;
                IList weight = (from i in vehicle.barbar
                                select new
                                {
                                    i.ID,
                                    i.Vehicle_Weight,
                                }).ToList();
                IList value = (from i in vehicle.barbar
                               select new
                               {
                                   i.ID,
                                   i.Vehicle_Value,
                               }).ToList();
                IList kind = (from i in pack.barbar
                              select new
                              {
                                  i.Id,
                                  i.TypeName,
                              }).ToList();


                Dictionary<string, IList> dd = new Dictionary<string, IList>();
                dd.Add("Destination", desti);
                dd.Add("Source", source);
                dd.Add("Weight", weight);
                dd.Add("Value", value);
                dd.Add("Kind", kind);
                return Json(dd, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        //
        // GET: /Index/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Index/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Index/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Index/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Index/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //Cannot open database "barbar.Models.vehicleContext" requested by the login. The login failed.<br>Login failed for user 'Tala-PC\Tala'.
        // POST: /Index/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace barbar.Controllers.Public
{
    public class PorterageInfoController : Controller
    {

        public JsonResult porterageinfo(int id)
        {
           

            Models.PorterageContext porterage = new Models.PorterageContext();
            Models.DestiContext destination = new Models.DestiContext();
            Models.packTypeContext packtype = new Models.packTypeContext();
            Models.PackTypePorterageContext porpacktype = new Models.PackTypePorterageContext();
            Models.DestiPorterageContext destipor = new Models.DestiPorterageContext();
            
             try
            {
                IList por = (from d in porterage.barbar where d.Id == id select new { d.Name  , d.OwnerName , d.PhoneNumber , d.Source , d.Rank , d.Address , d.Email}).ToList();
                List<int> despor = (from d in destipor.barbar where d.Porterage_ID == id select d.Desti_ID).ToList();
                IList des = null;
                  foreach (var d in despor)
                    des = (from de in destination.barbar where de.ID == d select de.Desti).ToList();
                List<int> packpor = (from d in porpacktype.barbar where d.IdPorterage == id select d.IdPacktype).ToList();
                IList pack  = null;
                foreach (var d in packpor)
                   pack = (from de in packtype.barbar where de.Id == d select de.TypeName).ToList();
            //    IList  = a.barbar.SqlQuery("Select * From Porterage where Code = id").ToList();
                Dictionary<string, IList> dd = new Dictionary<string, IList>();
                dd.Add("Porterage", por);
                dd.Add("Destination", des);
                dd.Add("PackType", pack);
                return Json(dd, JsonRequestBehavior.AllowGet);
            }
             catch (Exception ex)
             {
                 throw ex;
             }
        }

        public JsonResult request(int porId , int desId , int vehId , int packId, string de , string so ) {


            Dictionary<string, IList> answer = new Dictionary<string, IList>();
            try
            {
                if (HttpContext.Session["loggedIn"] != null)
                {
                    Models.PorterageContext por = new Models.PorterageContext();
                    Models.CustomerRequestContext crc = new Models.CustomerRequestContext();
                    Models.PricesContext p = new Models.PricesContext();
                    string username = HttpContext.Session["loggedIn"].ToString();
                    Models.CustomerRequest cr = new Models.CustomerRequest(porId, desId, vehId, packId , de , so ,username );
                    string source = (from i in por.barbar where i.Id == porId select i.Source).First();
                    IList po = (from i in por.barbar where i.Id == porId select i.Name).ToList();
                    int pric = (from i in p.barbar where i.vehicleID == vehId && i.destination == desId && i.source == source select i.price).First();
                    cr.price = pric;
                    crc.barbar.Add(cr);
                    crc.SaveChanges();
                    IList req = new List<int>();
                    req.Add(cr.Id);
                    crc.SaveChanges();
                    IList req1 = new List<int>();
                    req1.Add(cr.Status);
                    answer.Add("porterageName", po);
                    answer.Add("reqId", req);
                    answer.Add("reqStatus", req1);
                    return Json(answer, JsonRequestBehavior.AllowGet);

                }
                else
                    throw new Exception("لطفا وارد سایت شوید");
            }
            catch (Exception e) {
                throw e;
            }
        
        
        }
        //
        // GET: /PorterageInfo/


        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /PorterageInfo/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /PorterageInfo/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /PorterageInfo/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /PorterageInfo/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /PorterageInfo/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /PorterageInfo/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /PorterageInfo/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

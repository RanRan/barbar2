﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace barbar.Controllers.Public
{
    public class SignUpController : Controller
    {
        //
        // GET: /SignUp/
        public JsonResult Detail ()
        {
            try
            {
                Models.PorterageContext p = new Models.PorterageContext();
                Models.DestiContext desti = new Models.DestiContext();
                Models.packTypeContext pack = new Models.packTypeContext();
                Models.vehicleContext vehicle = new Models.vehicleContext();

           //     var pp = p.barbar.Find

                         
                IList kind = (from i in pack.barbar
                              select new
                                  {
                                      i.Id,
                                      i.TypeName,
                                  }).ToList();

                IList destii = (from i in desti.barbar
                                select new
                                {
                                    i.ID,
                                    i.Desti,
                                }).ToList();

                IList vehi = (from i in vehicle.barbar
                              select new
                              {
                                  i.ID,
                                  i.Vehicle_name,
                              }).ToList();
                Dictionary<string, IList> dd = new Dictionary<string, IList>();

                dd.Add("vehicle", vehi);
                dd.Add("destination", destii);
                dd.Add("packtype", kind);
                return Json(dd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
           

            
        }

        public ActionResult Index()
        {
            return View();
        }
        public JsonResult SignupAsCustomer(string fname, string lname, string password, string address, string emil, string phone, string username)
        {
            try
            {
                
                Models.userContext u = new Models.userContext();
                Models.User n = new Models.User(fname, lname, password, address, emil, phone, username);
                
                u.barbar.Add(n);
                u.SaveChanges();
               // Roles.AddUserToRole(n.UserName, "Customer");
               // n.Roles.Add()
                return null;
             }
            catch (Exception ex)
            {
                throw new Exception("نام کاربری موجود است");
            }
        }
        public void SignupPor(string code, string email, string owner, int ownerid, string name, string addre, string phone, string vehicle , string destination , string packType,string source)
        {
            int c = Int32.Parse(code);
            Models.PorterageContext u = new Models.PorterageContext();
        //    Models.vehicleContext v = new Models.vehicleContext();
            Models.PorterageVehicleContext p = new Models.PorterageVehicleContext();
            Models.DestiPorterageContext d = new Models.DestiPorterageContext();
            Models.PackTypePorterageContext pp = new Models.PackTypePorterageContext();
            Models.Porterage n;
            string Code;
            String Username;
            String Pass;
            string[] vehicles = vehicle.Split(',');
            string[] destis = destination.Split(',');
            string[] packs = packType.Split(',');
            try
            {

                n = new Models.Porterage(c, email, owner, ownerid, name, addre, phone, source);
                Code = n.Code.ToString();
                Username = Code;
               Random r = new Random();
               Pass = r.Next(10000, 99999).ToString() + (ownerid%100).ToString()+ Code[0] + Code[1];
                /* var md = new HMACMD5();
                 var salt = System.Text.Encoding.UTF8.GetBytes(Pass);
                 var Pass1 = md.ComputeHash(salt);*/
                n.UserName = Username;
                n.Password = Pass;
                u.barbar.Add(n);
                u.SaveChanges();
                for (int i = 0; i < vehicles.Length; i++)
                {

                    Models.PorterageVehicle o = new Models.PorterageVehicle(n.Id, Int32.Parse(vehicles[i]));
                    p.barbar.Add(o);
                   
                }


                for (int i = 0; i < destis.Length; i++)
                {

                    Models.DestiPorterage o = new Models.DestiPorterage(Int32.Parse(destis[i]), n.Id);
                    d.barbar.Add(o);
                   



                }


                for (int i = 0; i < packs.Length; i++)
                {

                    Models.PackTypePorterage o = new Models.PackTypePorterage(n.Id, Int32.Parse(packs[i]));
                    pp.barbar.Add(o);
                    



                }
                // Models.Vehicle m = new 
                
               
                pp.SaveChanges();
                d.SaveChanges();
                p.SaveChanges();
               // Roles.AddUserToRole(n.UserName, "Porterage");
               
            }
            catch (Exception e)
            {
                throw new Exception ("کد ثبت معتبر نیست");
                //return;
            }

           
           // v.barbar.Add(m);

            try
            {
                MailMessage mail = new MailMessage();


                mail.From = new MailAddress("raanasn@yahoo.de");
                mail.To.Add(new MailAddress(email));
                mail.Subject = ("نام کاربری و پسوورد باربری");
                mail.Body = "با سلام پسوورد شما" + Pass + "و نام کاربری شما" + Username;
                mail.IsBodyHtml = true;
                mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential("talayeh.riahi@gmail.com", "tala1374");// Enter seders User name and password
                smtp.EnableSsl = true;
                /*ServicePointManager.ServerCertificateValidationCallback =
                                        delegate(object s, X509Certificate certificate,
                                                 X509Chain chain, SslPolicyErrors sslPolicyErrors)
                                        { return true; };*/


                smtp.Send(mail);



            }
            catch (Exception ee)
            {
                throw new Exception("مشکل در فرستادن ایمیل");
            }


        }




        public ActionResult  logOut(){

            HttpContext.Session["LoggedIn"] = null;

            return Redirect("~/Public/Login");
        
        
        }

    }
}

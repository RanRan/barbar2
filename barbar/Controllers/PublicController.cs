﻿using barbar.Filters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace barbar.Controllers
{
    //[Authorize]
   // [InitializeSimpleMembership]
    public class PublicController : Controller
    {
        //
        // GET: /Public/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PorterageInfo()
        {
            ViewBag.Id=(Request.Params["Id"]);
            ViewBag.source = (Request.Params["source"]);
            ViewBag.desti = (Request.Params["desti"]);
            ViewBag.type = (Request.Params["type"]);
            ViewBag.weight = (Request.Params["weight"]);
            ViewBag.value = (Request.Params["value"]);
            ViewBag.Title = "مشخصات باربری";
            return View();
        }
        public ActionResult PorterageList()
        {
            return View();
        }
        //[AllowAnonymous]
        public ActionResult Login()
        {
            ViewBag.Title = "ورود";
            return View();
        }
        public ActionResult SignUp()
        {
            ViewBag.Title = "ثبت نام";
            return View();
        }
        public ActionResult Error4O4()
        {
            //ViewBag.Title = "ثبت نام";
            return View();
        }

    }
}

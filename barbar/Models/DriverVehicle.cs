using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Security;
namespace barbar.Models {
	public class DriverVehicle  {
        [Key]
        [Column(Order = 0)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Driver_ID { get; set; }

       // private Driver driver { get; set; }

	}
    public class DriverVehicleContext : DbContext
    {
        public DbSet<DriverVehicle> barbar { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DriverVehicle>().ToTable("DriverVehicle");


            base.OnModelCreating(modelBuilder);
        }
    }
}

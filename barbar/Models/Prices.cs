﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Security;

namespace barbar.Models
{
    public class Prices
    {
        public int destination { set; get; }
        public string source { set; get; }
        public int price { set; get; }
        [Key]
        [Column(Order = 0)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { set; get; }
        public int vehicleID { set; get; }

        public Prices(int des , string sourc , int p , int veh) {

            destination = des;
            source = sourc;
            price = p;
            vehicleID =veh;
            
        }

    }
    public class PricesContext : DbContext
    {
        public DbSet<Prices> barbar { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Prices>().ToTable("Prices");


            base.OnModelCreating(modelBuilder);
        }
    }
}
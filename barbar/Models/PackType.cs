using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Security;
namespace barbar.Models {
	public class PackType {
        [Key]
        [Column(Order = 0)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string TypeName { get; set; }
        public PackType(string typename)
            {
                TypeName = typename;
               
            }
	}
    public class packTypeContext : DbContext
    {
        public DbSet<PackType> barbar { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PackType>().ToTable("PackType");


            base.OnModelCreating(modelBuilder);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Security;

namespace barbar.Models
{
    public class DestiPorterage
    {
        [Key]
        [Column(Order = 0)]
        public int Desti_ID { get; set; }
        [Key]
        [Column(Order = 1)]
        public int Porterage_ID { get; set; }

        public DestiPorterage(int d, int p) {


            Desti_ID = d;
            Porterage_ID = p;
        
        }
    }
    public class DestiPorterageContext : DbContext
    {
        public DbSet<DestiPorterage> barbar { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DestiPorterage>().ToTable("DestiPorterage");


            base.OnModelCreating(modelBuilder);
        }
    }
}
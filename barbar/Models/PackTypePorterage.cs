﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Security;

namespace barbar.Models
{
    public class PackTypePorterage
    {
        [Key]
        [Column(Order = 0)]
        public int IdPorterage { get; set; }
        [Key]
        [Column(Order = 1)]
        public int IdPacktype { get; set; }
        public PackTypePorterage(int po,int pa)
        {
            IdPorterage = po;
            IdPacktype = pa;
        }
    }
    public class PackTypePorterageContext : DbContext
    {
        public DbSet<PackTypePorterage> barbar { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PackTypePorterage>().ToTable("PackTypePorterage");
      

            base.OnModelCreating(modelBuilder);
        }
    }
}
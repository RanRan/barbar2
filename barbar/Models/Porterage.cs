using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Security;

namespace barbar.Models {
  public class Porterage  {
      [Key]
      [Column(Order = 0)]
      [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Password { get; set; }
        public string Source { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
        public double Rank { get; set; }
      //  public barbar.Models.PackType[] PorteragePackType { get; set; }

        [Index(IsUnique = true)]
        public int Code { get; set; }
        public string OwnerName { get; set; }
        public int PorteragePriceId { get; set; }
        public int OwnerIdentityNumber { get; set; }
        //public string LogoImage { get; set; }
        public int packTypeId { get; set; }
     //   private Models.PorterageVehicle porterageVehicle { get; set; }
        public int porterageVehicleId { get; set; }
       // private PorteragePrice porteragePrice { get; set; }
        public int customerRequestId { get; set; }
        //private Models.Contract contract { get; set; }
        public int driverRequestId { get; set; }
        public string status { get; set; }
       // private Models.DriverRequest driverRequest { get; set; }
      //  private CustomerRequest customerRequest { get; set; }
     //   private PackType packType { get; set; }
        public Porterage(int code,string email, string owner,int ownerid,string name, string addre,string phone,string source) 
        {
            OwnerIdentityNumber = ownerid;
            //LogoImage = logo;
            Code = code;
            Email = email;
            OwnerName = owner;
            Name = name;
            Address = addre;
            PhoneNumber = phone;
            Source = source;
        }
  }
    public class PorterageContext : DbContext
    {
        public DbSet<Porterage> barbar { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Porterage>().ToTable("Porterage");
      

            base.OnModelCreating(modelBuilder);
        }
    }

}
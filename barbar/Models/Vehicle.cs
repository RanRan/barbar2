using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Security;

namespace barbar.Models {
	public class Vehicle {
        [Key]
        [Column(Order=0)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        
        public int Vehicle_Weight { get; set; }
        public int Vehicle_Value { get; set; }
        public string Vehicle_name { get; set; }
        public Vehicle(int weight,int value,string name)
        {
            Vehicle_Weight = weight;
            Vehicle_name = name;

            Vehicle_Value = value;


        }


	}
    public class vehicleContext : DbContext
    {
        public DbSet<Vehicle> barbar { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Vehicle>().ToTable("Vehicle");


            base.OnModelCreating(modelBuilder);
        }
    }

}

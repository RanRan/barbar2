using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Security;

namespace barbar.Models {
	public class User {
        public string Password { get; set; }
        [Key]
        [Column(Order = 0)]
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        
        //public int UserId { get; set; }
        public string Address { get; set; }

        public User(string fname, string lname, string password, string address, string emil, string phone, string username)
        {
            UserName = username;
            Email = emil;
            FirstName = fname;
            LastName = lname;
            PhoneNumber = phone;
            Address = address;
            Password = password;


        }

	}
    
    public class userContext : DbContext
    {
        public DbSet<User> barbar { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("User");


            base.OnModelCreating(modelBuilder);
        }
    }

}

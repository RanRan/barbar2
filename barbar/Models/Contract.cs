using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Security;

namespace barbar.Models {
	public class Contract {
        [Key]
        [Column(Order = 0)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Contract_ID { get; set; }
        public string Date { get; set; }
        public string Contract_Name { get; set; }
        public int Price { get; set; }
        public string Name_customer { get; set; }
        public string Body { get; set; }
        public string Register_name { get; set; }
        public int Porterage_Id { get; set; }


       // private barbar.Models.Porterage porterage { get; set; }

        public class ContractContext : DbContext
        {
            public DbSet<Contract> barbar { get; set; }
            protected override void OnModelCreating(DbModelBuilder modelBuilder)
            {
                modelBuilder.Entity<Contract>().ToTable("Contract");


                base.OnModelCreating(modelBuilder);
            }
        }

	}

}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Security;
namespace barbar.Models {
	public class DriverRequest {
        [Key]
        [Column(Order = 0)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int Driver_ID { get; set; }
        public int Porterage_ID { get; set; }

       // private barbar.Models.Porterage porterage { get; set; }
      //  private barbar.Models.Driver driver { get; set; }

	}
    public class DriverRequestContext : DbContext
    {
        public DbSet<DriverRequest> barbar { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DriverRequest>().ToTable("DriverRequest");


            base.OnModelCreating(modelBuilder);
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Security;
namespace barbar.Models {
	public class PorteragePrice {
        [Key]
        [Column(Order = 0)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int PorterageId { get; set; }
        public string Title { get; set; }
		public double Price{ get; set; }
      //  public Porterage Porterage { get; set; }

       // private Porterage porterage { get; set; }

	}
    public class PorteragePriceContext : DbContext
    {
        public DbSet<PorteragePrice> barbar { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PorteragePrice>().ToTable("PorteragePrice");


            base.OnModelCreating(modelBuilder);
        }
    }

}

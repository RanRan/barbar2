using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Security;
namespace barbar.Models {
    public class CustomerRequest
    {
        public string SourceAddress { get; set; }
        public int DestinationId { get; set; }
        //public PackType PackType { get; set; }
        public double Weight { get; set; }
        [Key]
        [Column(Order = 0)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int PackTypeId { get; set; }
        // public Customer Customer { get; set; }
        public string CustomerID { get; set; }
        public int VehicleId { get; set; }
        public int PorterageID { get; set; }
        public int price { get; set; }
        public string destiAddress { get; set; }

        /// if ==2 -> taeed customer }
        // public Porterage Porterage { get; set; }
        /// <summary>
        /// if ==0 -> taeed nashode
        /// if ==1 -> taeed barbari
        /// if ==3 -> taeed shode
        /// if ==4 -> laghv shode
        /// </summary>
        public int Status { get; set; }
        public CustomerRequest(int porId, int desId, int vehId, int packId , string des , string sou , string cus) {
            destiAddress = des;
            SourceAddress = sou;
            CustomerID = cus;
            DestinationId = desId;
            PorterageID = porId;
            VehicleId = vehId;
            PackTypeId = packId;
            Status = 0;
        }
        public CustomerRequest() { }
        //public int customerID { get; set; }
        // private Customer customer { get; set; }
        // private Porterage porterage { get; set; }

        // private PackType packType { get; set; }
    }
        public class CustomerRequestContext : DbContext
        {
            public DbSet<CustomerRequest> barbar { get; set; }
            protected override void OnModelCreating(DbModelBuilder modelBuilder)
            {
                modelBuilder.Entity<CustomerRequest>().ToTable("CustomerRequest");


                base.OnModelCreating(modelBuilder);
            }
        }

	

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Security;

namespace barbar.Models
{
    public class Destination
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string Desti { get; set; }

        public Destination(string name)
        {
            Desti = name;
        }

    }
    public class DestiContext : DbContext
    {
        public DbSet<Destination> barbar { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Destination>().ToTable("Destination");


            base.OnModelCreating(modelBuilder);
        }
    }
}
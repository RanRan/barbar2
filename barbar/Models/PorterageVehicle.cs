using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Security;
namespace barbar.Models {
	public class PorterageVehicle   {
        [Key]
        [Column(Order = 0)]
        public int Porterage_ID { get; set; }
        [Key]
        [Column(Order = 1)]
        public int Vehicle_ID { get; set; }

        public PorterageVehicle(int p, int v)
        {
            Porterage_ID = p;
            Vehicle_ID = v;
        }
      //  private barbar.Models.Porterage porterage { get; set; }

	}
    public class PorterageVehicleContext : DbContext
    {
        public DbSet<PorterageVehicle> barbar { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PorterageVehicle>().ToTable("PorterageVehicle");


            base.OnModelCreating(modelBuilder);
        }
    }

}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Security;
namespace barbar.Models {
	public class Driver  {
        [Key]
        [Column(Order = 0)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int License_code { get; set; }
        public int Traffic_certification_nr { get; set; }
        public string Vehicle_Right { get; set; }
        public int deriverRequestId { get; set; }
       // private Models.DriverRequest driverRequest { get; set; }
      //  private DriverVehicle driverVehicle { get; set; }
        public int deriverVehicleId { get; set; }
	}
    public class DriverContext : DbContext
    {
        public DbSet<Driver> barbar { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Driver>().ToTable("Driver");


            base.OnModelCreating(modelBuilder);
        }
    }

}
